package stack;

import java.util.ArrayList;

public class StackImpl implements Stack {
    ArrayList<StackItem> collection = new ArrayList<>();
    StackItem top = null;

    public void push(Object item) {
        StackItem newItem = new StackItem(item);
        collection.add(newItem);
        top = newItem;
    }

    public Object pop() {
        StackItem lastElement = top;
        collection.remove(top);
        if (!collection.isEmpty()) {
            top = collection.get(collection.size() - 1);
        }
        else
            top = null;
        return lastElement.getItem();
    }

    public boolean empty() {
        return top == null;
    }
}
